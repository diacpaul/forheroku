export type Sizes = "s"|"m"|"l"
export type Skin = "primary" | "highlight"| "danger" | "success"

export type Vehicle={
    id: number
    name:string
    manufactName: string
    manufactLogo: string
}

export type Tire={
    id: number
    brand:string
    season:string
    price:number
    size:string
}