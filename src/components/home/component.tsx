import * as React from "react"
import { Text, Icon } from "../_shared"
import { getAllVehicles } from "../../data/repositories/getAllVehicles/index"
import { Repositories } from "../../data/repositories"
import { Vehicle, Tire } from "../../data/model"
import VehicleTable from "./vehicleTable/component"
import { StoreType } from "../../business/model"
import { connect } from "react-redux"
import { Dispatch, bindActionCreators } from "redux"
import { IActions, Actions } from "../../business/actions"
import TiresTable from "./tyresTable/component"



type Props = StoreProps & DispatchProps & {

}

type DispatchProps = {
    actions: IActions
}

type StoreProps = {
    vehicleItems: Vehicle[]
    selectedVehicle?: Vehicle
    tires: Tire[]
    tiresLoading:boolean
}

class Home extends React.Component<Props> {



    componentDidMount() {
        this.props.actions.vehiclesLoading()
        Repositories.getAllVehicles().then(
            vehicles => this.props.actions.vehiclesLoaded(vehicles)
        )
    }

    handleVehicleSelect(vehicle: Vehicle) {
        const {actions}=this.props
        this.props.actions.selectVehicle(vehicle)
        Repositories.getTiresById(vehicle.id).then(
            tires => {
                actions.tiresLoaded(tires)
            }
        )

    }


    handleAddTireToBasket(tire:Tire){
        this.props.actions.addTiresToBAsket(tire)
    }
    render() {

        return (
            <div className="home ">
                
                <VehicleTable
                    onSelectVehicle={this.handleVehicleSelect.bind(this)}
                    vehicles={this.props.vehicleItems}
                    selectedVehicle={this.props.selectedVehicle}
                />

                {this.props.selectedVehicle &&
                    <TiresTable
                        tires={this.props.tires}
                        loading={this.props.tiresLoading}
                        onAddtoBasket={this.handleAddTireToBasket.bind(this)}
                    />}
            </div>
        )
    }
}


function mapStateToProps(store: StoreType): StoreProps {
    return {
        vehicleItems: store.vehicles.items,
        selectedVehicle: store.selectedVehicle,
        tires: store.tires.items,
        tiresLoading:store.tires.loading
    }
}


function mapDispatchToProps(dispatch: Dispatch): DispatchProps {
    return {
        actions: {
            vehiclesLoading: () => dispatch(Actions.vehiclesLoading()),
            // vehiclesLoaded :(vehicle:Vehicle[])=> dispatch(Actions.vehiclesLoaded(vehicle)


            vehiclesLoaded: bindActionCreators(Actions.vehiclesLoaded, dispatch),

            selectVehicle: bindActionCreators(Actions.selectVehicle, dispatch),
            tiresLoaded:bindActionCreators(Actions.tiresLoaded,dispatch),
            addTiresToBAsket:bindActionCreators(Actions.addTiresToBAsket,dispatch)


        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home) 