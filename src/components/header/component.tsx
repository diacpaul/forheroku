import * as React from "react"
import { Button , Text, Icon} from "../_shared"
import { RouteComponentProps, withRouter } from "react-router"
import { StoreType } from "../../business/model"
import { connect } from "react-redux"



type Props = RouteComponentProps & StoreProps & {


}

type StoreProps ={
    basketCount:number
}

class Header extends React.Component<Props> {
    render() {
        const {history} =this.props
        return (
         <div className="header">
             <div id="logo" onClick={()=>history.push("/")}></div>
            <Text strong size="l">Tyres Store by Diac Paul</Text>
            <Icon badge ={this.props.basketCount} name="basket" size="l" onClick={()=>history.push("/basket")}></Icon>
         </div>
        )
    }
}

function mapStateToPros(store:StoreType) : StoreProps{
    return{
        basketCount:store.basket.items.length
    }
}


export default connect(mapStateToPros)(withRouter(Header))