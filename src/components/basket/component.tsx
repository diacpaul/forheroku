import * as React from "react"
import { Button, Text, Icon } from "../_shared"
import { Tire } from "../../data/model"
import { connect } from "react-redux"
import { StoreType } from "../../business/model"



type Props = StoreProps & {


}

type StoreProps={
    basketItems: Tire[]
}

class Basket extends React.Component<Props> {
    render() {

        return (
            <div>
                <Text>
                    BASKET
                </Text>
                {this.props.basketItems.map(
                    basketItems=> <div>{basketItems.season}</div>
                )}
            </div>
        )
    }
}


function mapStateToPros(store:StoreType) : StoreProps{
    return{
        basketItems:store.basket.items 
    }
}



export default connect(mapStateToPros)(Basket)